#!/usr/bin/env luajit
setmetatable(_G, {__index = function (t,k) error("global read: "..k, 2) end})

local fmt = [[
#!/bin/sh
set -eu

[ "${1?no input}" = -a ] && exec find * -name '*.lua' -exec sh "$0" {} +

for file; do
	luajit -bl "$file" | awk '/ G[GS]ET / && !/; "(%s)"$/' | while read line; do
		printf '%%s:\t%%s\n' "$file" "$line"
	done
done]]


local globals = {}
for k in pairs(_G) do
	globals[#globals+1] = k
end

table.sort(globals)
print(fmt:format(table.concat(globals, "|")))
