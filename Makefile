all:	checkglobals.sh

%.sh:	%.lua
	luajit $< > $@
	chmod +x $@
